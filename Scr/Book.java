import java.time.LocalDate;

/**
 * Created by Julian Bischop on 18.12.2017.
 */
public class Book {
//Declaration
    private static int borrowWeeks = 2 , fees = 1;
    private int number;
    private String author, title;
    private LibUser user;
    private LocalDate returnDate;

//Constructor
    public Book(String author, String title){
        this.number = Main.getBooks().get(Main.getBooks().size()).getNumber()+1;
        this.author = author;
        this.title = title;

    }
/*
    GETTER AND SETTER
*/
//---Begin Getters
    public static int getBorrowWeeks() {
        return borrowWeeks;
    }

    public static int getFees() {
        return fees;
    }

    public int getNumber() {
        return number;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }
//---Begin Setter
    public static void setBorrowWeeks(int borrowWeeks) {
        Book.borrowWeeks = borrowWeeks;
    }

    public static void setFees(int fees) {
        Book.fees = fees;
    }

    public LibUser getUser() {
        return user;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    /*
    End GETTER AND SETTER
*/

    public boolean borrow(LibUser user){
        if(user.getblocked()) {
            return false;
        }
        else {
            this.user = user;
            returnDate = LocalDate.now().plusWeeks(getBorrowWeeks());
            System.out.println("Rückgabedatum: " + returnDate.toString());
            return true;
        }
    }

    public void returnBook(){
        this.user = null;
    }

}
