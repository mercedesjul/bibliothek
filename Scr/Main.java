import java.util.ArrayList;

/**
 * Created by Julian Bischop on 18.12.2017.
 */
public class Main {
    static ArrayList<Book> books = new ArrayList<>();
    static ArrayList<LibUser> users = new ArrayList<>();

    static public ArrayList<Book> getBooks() {
        return books;
    }

    static public ArrayList<LibUser> getUsers() {
        return users;
    }

    private void initialize(){
        books.add(new Book("Sir Readalot", "Gains by reading?!?"));
        books.add(new Book("Fore Elle","Die Lehren des Meeres"));
        books.add(new Book("SirLachsALot", "Freiheit in der Tiefsee - Eine kurze Novelle"));
        users.add(new LibUser(0,"Friedrich Spaten"));
        users.add(new LibUser(1,"De ela Grube"));
        users.add(new LibUser(2,"Ralf Rüdiger"));
    }
    public void ausleihen(int KundenNummer, int BuchNummer){
        Book book = null;
        LibUser user = null;
        for(int i = 0; i < books.size();i++){
            if(books.get(i).getNumber() == BuchNummer){
                book = books.get(i);
            }
        }
        for(int i = 0; i < users.size();i++){
            if(users.get(i).getnumber() == KundenNummer){
                user = users.get(i);
            }
        }
        try {
            book.borrow(user);
        }
        catch(NullPointerException e){
            System.out.println("User oder Buch nicht gefunden");
        }
    }
    public void rückgabe(int buchNummer){
        Book book = null;
        for(int i = 0; i < books.size();i++){
            if(books.get(i).getNumber() == buchNummer){
                book = books.get(i);
            }
        }
        LibUser user = book.getUser();

    }
}
