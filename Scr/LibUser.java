//created by Florian Sauer 20.12.2017

public class LibUser {

  private String name;
  private int number;
  private int fees = 0;
  private boolean blocked = false;
  private static int maxFees = 3;
  
  public LibUser(){
  
   }

  public LibUser(int number, String name) {
    this.number = number;
    this.name = name; 
  }
    
  public String getname() {return name;}
  public int getnumber() {return number;}
  public int getfees() {return fees;}
  public boolean getblocked() {return blocked;}
  public static int getmaxFees() {return maxFees;}
        
  public static void setmaxFees(int maxFees) {
    LibUser.maxFees = maxFees;
    }
  public void setFees(int fees) {
    this.fees = fees;
    }  

  public boolean isBlocked() {
    return this.blocked;    
    }
  
  public void block() {
    blocked = true;
    }
  public void unblock() {
    blocked = false;
    }  
    
  }